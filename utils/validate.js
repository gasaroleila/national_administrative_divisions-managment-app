const joi = require('joi');

module.exports.Schema = joi.object({
    provinceCode: joi.string()
        .required()
        .alphanum()
        .length(5),

    provinceName: joi.string()
        .required()
        .alphanum()
        .min(10)
        .max(100),

    provLeader: joi.string()
        .required()
        .alphanum()
        .pattern(new RegExp('[a-zA-Z]'))
        .min(15)
        .max(100),

    provPopulation: joi.number()
        .required()
        .greater(0),

    districtNum: joi.number()
        .required()
        .min(3),

    provSpecialisation: joi.string()
        .alphanum()
        .min(0)
        .max(100)
})