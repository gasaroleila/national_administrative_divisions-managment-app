const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');

const config = require('config');
require('./Models/mongodb');

const app = express();
const districtController = require('./Controllers/district.Controller');
const provinceController = require('./Controllers/province.Controller');


app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

app.use((err, req, res, next) => {
  if (err.httpStatusCode === 404) {
      res.status(400).send('NotFound');
      dbErrors("Not found")
  }
  next(err);
});
app.use((err, req, res, next) => {
  if (err.httpStatusCode === 304) {
      res.status(304).send('Unauthorized');
      dbErrors("Unauthorized")
  }
  next(err);
})
// catch all the other errors
app.use((err, req, res, next) => {
  log(err);
  if (!res.headersSent) {
      res.status(err.httpStatusCode || 500).send('UnknownError');
      dbErrors("Unknown error");
  }
});

let logging = require('debug')('message');

function logger(req,res) {
    return logging(req);
}


app.get('/', (req, res) => {
  res.send('Welcome to Rwanda National Administration division managment app')
});

app.use('/api/districts', districtController);
app.use('/api/provinces', provinceController);

console.log(`name in development ${config.get("application.port")}`);

const port = process.env.PORT || 2020;
app.listen(port, () => console.log(`Listening on port ${port}..`));