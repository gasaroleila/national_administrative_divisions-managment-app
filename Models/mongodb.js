const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/managment', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(()=>{
    console.log("CONNECTED SUCCESSFULLY");
}).catch((err)=>{
    console.log(" Failed to connect to mongodb, error: "+err);
});