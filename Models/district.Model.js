const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const DistrictSchema = new Schema({
    districtCode: {
        type:String,
        required:true
    },
    districtName: {
        type: String,
        required: true
    },

    districtLeader: {
        type: String,
        required: true
    },

    provinceCode: {
        type:String,
        required:true,
        $ref:"provinces"
    }
});

module.exports = mongoose.model('Districts', DistrictSchema);