const mongoose = require('mongoose');

const ProvinceSchema = new mongoose.Schema({
    provinceCode: {
        type:String,
        required:true
    },
    provinceName: {
    type: String,
    required: true
    },
    provLeader: {
        type:String,
        required:true
    },
    provPopulation: {
    type: Number,
    required: true
    },
    districtNum: {
        type: Number,
        required:true
    },
    provSpecialisation: {
        type:String,
        required:true
    }
    
});
module.exports = mongoose.model('Provinces', ProvinceSchema);
