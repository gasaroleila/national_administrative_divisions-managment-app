const express = require('express');

const router = express.Router();

const Province = require('../Models/province.Model');
const District = require('../Models/district.Model');



router.get('/:provinceCode', async(req,res) => {
    const provCode = req.params.provinceCode;
    District.find({provinceCode: provCode})
      .then(districts => {
          res.status(200).send(districts);
      }).catch(err => {
          res.status(400).send(err);
      })
    
})


router.get('/:districtCode', async (req,res,next) => {
const distCode = req.params.districtCode;
  District.find({districtCode:distCode}) 
     .then(district => {
         res.status(200).send(district);
     }).catch(err => {
         res.status(400).send(err);
     })
})


router.post('/', async(req,res) => {
try {
    const district = await District.findOne({DistrictName: req.body.DistrictName});
    if(province)
        return res.send('District already exists');


    const errorCheck = await util.Schema.validate(req.body);
    if(errorCheck.error)
        return res.status(400).send(errorCheck.error.details[0].message);


    const district1 = new District({
     districtCode:req.body.districtCode,
     districtName: req.body.districtName,
     districtLeader: req.body.districtLeader,
     provinceCode: req.body.provinceCode

    })

    return district1.save()
        .then(result => {
            res.status(201).send('(1) District was created!');
        })

}catch(err) {
    console.log(err);
    return res.status(400).send({error: err});
}

})


router.put('/:districtCode',async(req,res) => {
const distCode = req.params.districtCode;

try {
  const errorCheck = await util.Schema.validate(req.body);
  if(errorCheck.error)
      return res.status(400).send(errorCheck.error.details[0].message);

  District.findByIdAndUpdate(distCode,
     req.body, 
     {
         new: true
        },
    (error, doc) => {
        if(error)
            return res.status(404).send(error);

        res.status(201).send(doc);

    });
} catch (error) {
  res.status(400).send(error);
}

})


router.delete('/:districtCode',(req,res) => {
const distCode = req.params.districtCode;

try {
    District.findByIdAndDelete(distCode, 
        {
        new: false
    },

    (err,doc) => {
        if(err)
            return err;

        res.status(201).send(`${doc.districtName} has been deleted!`);
    })
}

catch(err) {
    res.status(400).send(err);
}

})


module.exports = router;
