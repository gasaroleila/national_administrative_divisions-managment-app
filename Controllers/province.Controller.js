const express = require('express');

const router = express.Router();

const Province = require('../Models/province.Model');
const District = require('../Models/district.Model');

const util = require('../utils/validate');


router.get('/', async(req,res) => {
        try{ 
            const provinces = await Province.find();
            return res.status(200).send(provinces); 
        }
        catch(err){
            res.status(404).send(err);
        }
})


router.get('/:provinceCode', async (req,res,next) => {
    const provCode = req.params.provinceCode;
    try { 
        const province = await Province.find({provinceCode: provCode});
        return res.status(200).send(province);
    }

    catch(error) {
        res.status(400).send(error);
    }
})


router.post('/', async(req,res) => {
    try {
        const province = await Province.findOne({provinceName: req.body.provinceName});
        if(province)
            return res.send('Province already exists');


        const errorCheck = await util.Schema.validate(req.body);
        if(errorCheck.error)
            return res.status(400).send(errorCheck.error.details[0].message);


        const province1 = new Province({
          provinceCode: req.body.provinceCode,
          provinceName: req.body.provinceName,
          provLeader:req.body.provLeader,
          provPopulation: req.body.provPopulation,
          districtNum: req.body.districtNum,
          provSpecialisation: req.body.provSpecialisation

        })

        return province1.save()
            .then(result => {
                res.status(201).send('(1) Province was created!');
            })

    }catch(err) {
        console.log(err);
        return res.status(400).send({error: err});
    }

})


router.put('/:provId',async(req,res) => {
    const provId = req.params.provId;

  try {
      const errorCheck = await util.Schema.validate(req.body);
      if(errorCheck.error)
          return res.status(400).send(errorCheck.error.details[0].message);

      Province.findByIdAndUpdate(provId,
         req.body, 
         {
             new: true,
             useFindAndModify:false
            },
        (error, doc) => {
            if(error)
              return res.status(404).send(error);

            res.status(201).send(doc);

        });
  } catch (error) {
      res.status(400).send(error);
  }

})


router.delete('/:provId',(req,res) => {
    const provId = req.params.provId;

    try {
        Province.findByIdAndDelete(provId, 
            {
            new: false
        },

        (err,doc) => {
            if(err)
                return err;

            res.status(201).send(`1 province has been deleted!`);
        })
    }

    catch(err) {
        res.status(400).send(err);
    }

});



function logger() {
    
}


module.exports = router;



